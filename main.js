var mydata = JSON.parse(data);
var contenido = document.querySelector("#contenido");
var selector = document.getElementById("select");

for (let valor of mydata) {
  let option = document.createElement("option");
  option.value = valor.Apellidos;
  option.innerText = valor.Apellidos;
  selector.appendChild(option);
}

function mostrar(apellido) {
  contenido.innerHTML = "";
  for (let valor of mydata) {
    if (apellido.value == valor.Apellidos) {
      contenido.innerHTML += `

                      <tr>
                          <td scope="row">${valor.Apellidos}</td>
                          <td>${valor.Nombres}</td>
                          <td>${valor.Semestre}</td>
                          <td>${valor.Paralelo}</td>
                          <td>${valor.Dirección}</td>
                          <td>${valor.Teléfono}</td>
                          <td>${valor.Email}</td>
                      </tr>

                      `;
    }
  }
}
