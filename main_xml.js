var xml =
  "<lista>  <estudiante>      <Apellidos>Santacruz</Apellidos>      <Nombres>Alex David</Nombres>      <Semestre>Quinto Semestre</Semestre>      <Paralelo>A</Paralelo>      <Dirección>Av. 9 de Julio y Olmedo</Dirección>      <Teléfono>0959723655</Teléfono>      <Email>alexdavid_95_@hotmail.com</Email>  </estudiante>  <estudiante>      <Apellidos>Zuckerberg</Apellidos>      <Nombres>Mark Elliot</Nombres>      <Semestre>Primer Semestre</Semestre>      <Paralelo>B</Paralelo>      <Dirección>White Plains, Nueva York</Dirección>      <Teléfono>0981274412</Teléfono>      <Email>markzuckerberg@facebook.com</Email>  </estudiante>  <estudiante>      <Apellidos>Bezos</Apellidos>      <Nombres>Jeffrey Preston</Nombres>      <Semestre>Segundo Semestre</Semestre>      <Paralelo>A</Paralelo>      <Dirección>Albuquerque, Nuevo México</Dirección>      <Teléfono>0912519516</Teléfono>      <Email>jeffbezos@amazon.com</Email>  </estudiante>  <estudiante>      <Apellidos>Page</Apellidos>      <Nombres>Lawrence Edward</Nombres>      <Semestre>Tercer Semestre</Semestre>      <Paralelo>C</Paralelo>      <Dirección>East Lansing, Michigan</Dirección>      <Teléfono>0987712902</Teléfono>      <Email>larrypage@google.com</Email>  </estudiante>  <estudiante>      <Apellidos>Brin</Apellidos>      <Nombres>Serguéi Mijáilovich</Nombres>      <Semestre>Cuarto Semestre</Semestre>      <Paralelo>D</Paralelo>      <Dirección>Moscú, Rusia</Dirección>      <Teléfono>0962412002</Teléfono>      <Email>sergeybrin@google.com</Email>  </estudiante>  <estudiante>      <Apellidos>Jobs</Apellidos>      <Nombres>Steven Paul</Nombres>      <Semestre>Sexto Semestre</Semestre>      <Paralelo>A</Paralelo>      <Dirección>San Francisco, California</Dirección>      <Teléfono>0989230930</Teléfono>      <Email>stevejobs@apple.com</Email>  </estudiante>  <estudiante>      <Apellidos>Musk</Apellidos>      <Nombres>Elon Reeve</Nombres>      <Semestre>Septimo Semestre</Semestre>      <Paralelo>B</Paralelo>      <Dirección>Pretoria, Sudáfrica</Dirección>      <Teléfono>0934892130</Teléfono>      <Email>elonmusk@tesla.com</Email>  </estudiante>  <estudiante>      <Apellidos>Nadella</Apellidos>      <Nombres>Satya</Nombres>      <Semestre>Octavo Semestre</Semestre>      <Paralelo>D</Paralelo>      <Dirección>Hyderabad, India</Dirección>      <Teléfono>0932902390</Teléfono>      <Email>satyanadella@microsoft.com</Email>  </estudiante>  <estudiante>      <Apellidos>Sundarajan</Apellidos>     <Nombres>Pichai</Nombres>      <Semestre>Noveno Semestre</Semestre>      <Paralelo>B</Paralelo>      <Dirección>Madurai, India</Dirección>      <Teléfono>0915121239</Teléfono>      <Email>sundarpichai@google.com</Email>  </estudiante>  <estudiante>      <Apellidos>Gates III</Apellidos>      <Nombres>William Henry</Nombres>      <Semestre>Decimo Semestre</Semestre>      <Paralelo>C</Paralelo>      <Dirección>Seattle, Washington</Dirección>      <Teléfono>0998123782</Teléfono>      <Email>billgates@microsoft.com</Email>  </estudiante></lista>";
let parser = new DOMParser();
let xmlDOM = parser.parseFromString(xml, "application/xml");
let estudiantes = xmlDOM.querySelectorAll("estudiante");

var selectorXML = document.getElementById("select-xml");
var contenidoXML = document.querySelector("#contenido-xml");

for (let valor of estudiantes) {
  let option = document.createElement("option");
  option.value = valor.children[0].textContent;
  option.innerText = valor.children[0].textContent;
  selectorXML.appendChild(option);
}

function mostrarXML(apellidoXML) {
  contenidoXML.innerHTML = "";
  for (let valor of estudiantes) {
    if (apellidoXML.value == valor.children[0].textContent) {
      contenidoXML.innerHTML += `

                        <tr>
                            <td scope="row">${valor.children[0].textContent}</td>
                            <td>${valor.children[1].textContent}</td>
                            <td>${valor.children[2].textContent}</td>
                            <td>${valor.children[3].textContent}</td>
                            <td>${valor.children[4].textContent}</td>
                            <td>${valor.children[5].textContent}</td>
                            <td>${valor.children[6].textContent}</td>
                        </tr>

                        `;
    }
  }
}
